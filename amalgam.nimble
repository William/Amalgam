# Package

version = "0.1.0"
author = "william behrens"
description = "A text editor written in nim using fidget"
license = "GPLv3"
srcDir = "src"
bin = @["main"]


# Dependencies

requires "nim >= 1.4.2"
requires "fidget"
